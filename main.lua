local Server = require("HTTP.HTTPproxyServer")

function main(Data)
   local Request = Server.parseRequest(Data)
   local Body
   if(Request.path == "/") then 
      Body = "<h1>Hello</h1>"
   else 
      Body = "<h1>Goodbye</h1>" 
   end
   Server.respond{code=200, headers={}, body=Body}
end

-- TODO move this out of main.
local Configs = component.fields();
local Settings = {
   port = Configs.WebPort,
   ssl = {
      cert = Configs.SSLcert,
      key = Configs.SSLkey
   }
}
Server.start(Settings)
